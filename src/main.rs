#[macro_use]
extern crate serde_derive;
extern crate html_escape;
use actix_files as fs;
use actix_web::{get, http, web, App, HttpResponse, HttpServer, Responder, Result as ActixResult};
use std::path::PathBuf;
use std::sync::Mutex;
mod command;
mod game;
mod gamestage;
mod user;
mod wrong_guess;
use command::GameCommand;
use game::GameState;
use rand::distributions::Alphanumeric;
use rand::{thread_rng, Rng};

#[get("/")]
/// Redirect from root to the index
async fn to_index() -> impl Responder {
    HttpResponse::Found()
        .header("Location", "/static/index.html")
        .finish()
}

struct GameStateHolder {
    state: Mutex<GameState>,
    salt: String,
    admin_pass: String,
}

impl GameStateHolder {
    fn new() -> Self {
        let admin_pass = thread_rng().sample_iter(&Alphanumeric).take(30).collect();
        println!("Admin pass: {}", admin_pass);
        Self {
            state: Mutex::new(GameState::new()),
            salt: thread_rng().sample_iter(&Alphanumeric).take(30).collect(),
            admin_pass,
        }
    }
}

#[derive(Deserialize)]
struct Login {
    secret: String,
}
async fn get_status(data: web::Data<GameStateHolder>, info: web::Path<Login>) -> impl Responder {
    if let Ok(mut gamestate) = data.state.lock() {
        if GameCommand::check_secret(&info.secret) && gamestate.is_user(&info.secret) {
            // Update turns etc.
            gamestate.update();

            HttpResponse::Ok()
                .header(http::header::CONTENT_TYPE, "application/json")
                .body(gamestate.to_json(&info.secret))
        } else {
            HttpResponse::Forbidden().body("NO ACCESS")
        }
    } else {
        HttpResponse::InternalServerError().body("No unlock")
    }
}

#[derive(Serialize)]
struct ErrorStr {
    error: &'static str,
}
fn error_to_json(error: &'static str) -> String {
    serde_json::to_string(&ErrorStr { error }).unwrap()
}

async fn get_command(req_body: String, data: web::Data<GameStateHolder>) -> impl Responder {
    if let Ok(command) = serde_json::from_str::<GameCommand>(&req_body) {
        if let Ok(mut gamestate) = data.state.lock() {
            if !command.is_clean() {
                return HttpResponse::BadRequest()
                    .header(http::header::CONTENT_TYPE, "application/json")
                    .body(error_to_json("Bad field in request"));
            }
            match gamestate.receive_command(command) {
                Ok(()) => HttpResponse::Ok()
                    .header(http::header::CONTENT_TYPE, "application/text")
                    .body("OK"),
                Err(val) => HttpResponse::InternalServerError().body(error_to_json(val)),
            }
        } else {
            HttpResponse::InternalServerError().body("No unlock")
        }
    } else {
        HttpResponse::BadRequest().body("{'error': 'Could not decode JSON'}")
    }
}

async fn salt(data: web::Data<GameStateHolder>) -> impl Responder {
    HttpResponse::Ok()
        .header(http::header::CONTENT_TYPE, "application/text")
        .body(data.salt.clone())
}

#[derive(Deserialize)]
#[serde(tag = "command")]
enum AdminCommand {
    MakeMaster { pass: String, secret: String },
    Kick { pass: String, secret: String },
    GetInfo { pass: String },
}
async fn admin_page(req_body: String, data: web::Data<GameStateHolder>) -> impl Responder {
    if let Ok(adcom) = serde_json::from_str::<AdminCommand>(&req_body) {
        if let Ok(mut gamestate) = data.state.lock() {
            match adcom {
                AdminCommand::GetInfo { pass } => {
                    if pass == data.admin_pass {
                        HttpResponse::Ok()
                            .header(http::header::CONTENT_TYPE, "application/json")
                            .body(gamestate.admin_json())
                    } else {
                        HttpResponse::Forbidden().body("NO ACCESS")
                    }
                }
                AdminCommand::MakeMaster { pass, secret } => {
                    if pass == data.admin_pass {
                        gamestate.make_user_master(&secret);
                        HttpResponse::Ok()
                            .header(http::header::CONTENT_TYPE, "application/json")
                            .body(gamestate.admin_json())
                    } else {
                        HttpResponse::Forbidden().body("NO ACCESS")
                    }
                }
                AdminCommand::Kick { pass, secret } => {
                    if pass == data.admin_pass {
                        gamestate.kick_user(&secret);
                        HttpResponse::Ok()
                            .header(http::header::CONTENT_TYPE, "application/json")
                            .body(gamestate.admin_json())
                    } else {
                        HttpResponse::Forbidden().body("NO ACCESS")
                    }
                }
            }
        } else {
            HttpResponse::InternalServerError().body("No unlock")
        }
    } else {
        HttpResponse::BadRequest().body("Could not decode Json")
    }
}

async fn favicon() -> ActixResult<fs::NamedFile> {
    let path = PathBuf::from(r"static/favicon.ico");
    Ok(fs::NamedFile::open(path)?)
}

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    let state = web::Data::new(GameStateHolder::new());
    HttpServer::new(move || {
        App::new()
            .app_data(state.clone())
            .service(to_index)
            .service(fs::Files::new("/static", "static").show_files_listing())
            .route("/favicon.ico", web::get().to(favicon))
            .service(web::resource("/user/{secret}/status").route(web::get().to(get_status)))
            .service(web::resource("/command").route(web::post().to(get_command)))
            .service(web::resource("/admin").route(web::post().to(admin_page)))
            .service(web::resource("/salt").route(web::get().to(salt)))
    })
    .bind("192.168.2.224:5555")?
    .run()
    .await
}
