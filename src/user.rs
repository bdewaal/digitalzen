use html_escape::encode_safe;
use serde::Serializer;

pub type Secret = String; // We store students as their secret

#[derive(Serialize)]
/// Struct for storing the users (either master or students)
/// Initiative is the order in which their next turn is, lowest first
pub struct User {
    #[serde(serialize_with = "serialize_string_escape")]
    pub username: String,
    pub secret: Secret,
    pub initiative: u16,        // Initiative within the round
    pub master_initiative: u16, // Initiative to be the next master
    pub guess_tokens: u16,
    pub wins: u16,
    pub master: bool,
    pub turn: bool, // Is it this players turn? Set using the update function
}

impl User {
    /// Create a new user
    /// Note: We assume the username and "secret" have already been checked for correctness
    pub fn new(username: String, secret: Secret) -> Self {
        Self {
            username,
            secret,
            initiative: 0,
            master_initiative: 0,
            guess_tokens: 0,
            wins: 0,
            master: false,
            turn: false,
        }
    }

    /// Create a "public" version of this user, for easy export to the users
    /// "secret" is used to identify the "Me" function
    pub fn public(&self, secret: &str) -> PublicUser {
        PublicUser {
            username: encode_safe(&self.username).to_string(),
            master: self.master,
            me: secret == self.secret,
            turn: self.turn,
            guess_tokens: self.guess_tokens,
            wins: self.wins,
        }
    }
}

/// Trait for managing collection of user
/// Used so we can define the user management functions here
pub trait UserCollection {
    fn find_user_mut(&mut self, secret: &str) -> Option<&mut User>;
    fn find_user(&self, secret: &str) -> Option<&User>;
    fn is_master(&self, secret: &str) -> bool;
    fn exists(&self, secret: &str) -> bool;
    fn username(&self, secret: &str) -> String;
    fn next_user(&self) -> Option<String>;
    fn add_user(&mut self, secret: Secret, username: String) -> bool;

    /// Sort the users so the master is first, then all the users
    fn order(&mut self);

    /// Return the next highest initiative
    /// So used when you want to move someone to the end of the line
    fn next_initiative(&self) -> u16;
    fn next_master_initiative(&self) -> u16;
}

impl UserCollection for Vec<User> {
    fn find_user_mut(&mut self, secret: &str) -> Option<&mut User> {
        self.iter_mut().find(|user| user.secret == secret)
    }

    fn find_user(&self, secret: &str) -> Option<&User> {
        self.iter().find(|user| user.secret == secret)
    }

    fn is_master(&self, secret: &str) -> bool {
        self.find_user(secret).map(|u| u.master).unwrap_or(false)
    }

    fn exists(&self, secret: &str) -> bool {
        self.find_user(secret).is_some()
    }

    fn username(&self, secret: &str) -> String {
        encode_safe(
            self.find_user(secret)
                .map(|x| x.username.as_str())
                .unwrap_or("Anon"),
        )
        .to_string()
    }

    fn next_user(&self) -> Option<String> {
        self.iter()
            .filter(|u| !u.master)
            .min_by_key(|u| u.initiative)
            .map(|u| u.secret.clone())
    }

    fn next_initiative(&self) -> u16 {
        self.iter().map(|u| u.initiative).max().unwrap_or(0) + 1
    }

    fn next_master_initiative(&self) -> u16 {
        self.iter().map(|u| u.master_initiative).max().unwrap_or(0) + 1
    }

    /// Add a user
    /// If the user already exists, only change the username
    /// If there was no user yet, make them the master
    /// Returns whether it was successful
    fn add_user(&mut self, secret: Secret, username: String) -> bool {
        // if the user exists we only overwrite the name
        if let Some(mut user) = self.find_user_mut(&secret) {
            user.username = username;
        } else {
            self.push(User::new(username, secret));

            // If we only have one user, make them a master
            if self.len() == 1 {
                self.iter_mut().for_each(|u| u.master = true);
            }
        }
        true
    }

    fn order(&mut self) {
        self.sort_by_key(|u| if u.master { -1 } else { u.initiative as i32 });
    }
}

#[derive(Serialize)]
/// Public version of user, used for easy serialization
pub struct PublicUser {
    username: String,
    master: bool,
    me: bool,   // Whether this is the current user
    turn: bool, // Whether it's this players turn
    guess_tokens: u16,
    wins: u16,
}

/// Serializer for Serde that HTML escapes strings
/// Used for escaping the username of the users when serializing them
fn serialize_string_escape<S>(string: &str, serializer: S) -> Result<S::Ok, S::Error>
where
    S: Serializer,
{
    serializer.serialize_str(&encode_safe(string))
}
