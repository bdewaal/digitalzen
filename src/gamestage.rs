use crate::game::{Field, ROUNDTIME};
use crate::user::{Secret, User, UserCollection};
use html_escape::encode_safe;
use std::time::Instant;

fn seconds_remaining(start: &Instant) -> u16 {
    let remaining = ROUNDTIME - (start.elapsed().as_secs() as i32);
    if remaining > 0 {
        remaining as u16
    } else {
        0
    }
}

#[derive(Clone)]
pub enum GameStage {
    MasterMakesRule,
    MasterBuildingExamples,
    StudentBuildsField(Secret, Instant), // Will time out
    AnyStudentBuildsField,
    MasterJudges(Secret, Field), // Field to judge, secret of student
    MondoVoting(Secret, Field, Vec<(Secret, bool)>, Instant), // Vec contains true/false guesses. Step times out after a while
    MasterJudgesMondo(Secret, Field, Vec<(Secret, bool)>),
    StudentGuesses(Secret, Instant), // Times out
    MasterJudgesGuess(Secret, String),
    StudentWon(Secret, Instant, String, Vec<Secret>), // Times out, game restarts afterwards! Secret only there to show result.
                                                      // The Vec holds the Students that are willing to be master next
}

impl GameStage {
    pub fn new() -> Self {
        Self::MasterMakesRule
    }

    /// Produce a 'public' version of the `GameStage`, so we can serialize it using Serde
    // allow ptr_arg, because we are using methods we defined on Vec<User>
    #[allow(clippy::ptr_arg)]
    pub fn public(&self, users: &Vec<User>) -> PublicGameStage {
        match self {
            Self::MasterMakesRule => PublicGameStage::MasterMakesRule,
            Self::MasterBuildingExamples => PublicGameStage::MasterBuildingExamples,
            Self::StudentBuildsField(s, i) => PublicGameStage::StudentBuildsField {
                username: users.username(s),
                remaining_time: seconds_remaining(i),
            },
            Self::AnyStudentBuildsField => PublicGameStage::AnyStudentBuildsField,
            Self::MasterJudges(s, f) => PublicGameStage::MasterJudges {
                username: users.username(s),
                field: f.clone(),
            },
            Self::MondoVoting(s, f, _, i) => PublicGameStage::MondoVoting {
                username: users.username(s),
                field: f.clone(),
                remaining_time: seconds_remaining(i),
            },
            Self::MasterJudgesMondo(s, f, guesses) => PublicGameStage::MasterJudgesMondo {
                username: users.username(s),
                field: f.clone(),
                user_guesses: guesses
                    .iter()
                    .map(|(s, val)| (users.username(s), *val))
                    .collect(),
            },
            Self::StudentGuesses(s, i) => PublicGameStage::StudentGuesses {
                username: users.username(s),
                remaining_time: seconds_remaining(i),
            },
            Self::MasterJudgesGuess(s, guess) => PublicGameStage::MasterJudgesGuess {
                username: users.username(s),
                guess: encode_safe(guess).to_string(),
            },
            Self::StudentWon(s, i, guess, vu) => PublicGameStage::StudentWon {
                username: users.username(s),
                remaining_time: seconds_remaining(i),
                guess: encode_safe(guess).to_string(),
                volunteers: vu.iter().map(|u| users.username(u)).collect(),
            },
        }
    }
}

#[derive(Serialize)]
#[serde(tag = "stage")]
pub enum PublicGameStage {
    MasterMakesRule,
    MasterBuildingExamples,
    StudentBuildsField {
        username: String,
        remaining_time: u16,
    }, // Will time out
    AnyStudentBuildsField,
    MasterJudges {
        username: String,
        field: String,
    },
    MondoVoting {
        username: String,
        field: String,
        remaining_time: u16,
    }, // Vec contains true/false guesses. Step times out after a while
    MasterJudgesMondo {
        username: String,
        field: String,
        user_guesses: Vec<(String, bool)>,
    },
    StudentGuesses {
        username: String,
        remaining_time: u16,
    }, // Times out
    MasterJudgesGuess {
        username: String,
        guess: String,
    },
    StudentWon {
        username: String,
        guess: String,
        remaining_time: u16,
        volunteers: Vec<String>,
    }, // Times out, game restarts afterwards!
}
