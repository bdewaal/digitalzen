use crate::command::ReasonGuessWrong;
use crate::game::Field;
use crate::user::{Secret, User, UserCollection};
use html_escape::encode_safe;

/// Wrong guess
/// Used to store wrong guesses so they can be viewed back later
pub struct WrongGuess {
    secret: Secret,
    field: Field,
    guess: String,
    reason: ReasonGuessWrong,
}

#[derive(Serialize)]
/// Public version of wrong guess, with outputs suitable for serializing and sending to players
pub struct PublicWrongGuess {
    username: String,
    field: Field,
    guess: String,
    reason: ReasonGuessWrong,
}

impl WrongGuess {
    pub fn new(secret: Secret, field: Field, guess: String, reason: ReasonGuessWrong) -> Self {
        Self {
            secret,
            field,
            guess,
            reason,
        }
    }

    /// Produce a public version, suitable for serializing with Serde
    // allow ptr_arg, because we are using methods we defined on Vec<User>
    #[allow(clippy::ptr_arg)]
    pub fn public(&self, users: &Vec<User>) -> PublicWrongGuess {
        PublicWrongGuess {
            username: users.username(&self.secret),
            field: self.field.clone(),
            guess: encode_safe(&self.guess).to_string(),
            reason: self.reason,
        }
    }
}
