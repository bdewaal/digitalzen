use crate::game::Field;
use crate::user::Secret;

const MAX_USERNAME_LEN: usize = 50;
const MAX_GUESS_LEN: usize = 2000;

#[derive(Deserialize)]
#[serde(tag = "command")]
pub enum GameCommand {
    AddUser {
        secret: Secret,
        username: String,
    },
    /// The master submits the rule that the rest must guess
    SubmitRule {
        secret: Secret,
        rule: String,
    },
    /// The master may always submit a field
    SubmitFieldAsMaster {
        secret: Secret,
        field: Field,
        buddha: bool,
    },
    /// Allows a user to skip any command with a countdown
    Skip {
        secret: Secret,
    },
    SubmitFieldForMondo {
        secret: Secret,
        field: Field,
    },
    SubmitFieldForJudgement {
        secret: Secret,
        field: Field,
    },
    /// Can be done by users for a mondo, or by the master (after a mondo or judgment is
    /// required).
    ClassifyField {
        secret: Secret,
        buddha: bool,
    },
    SubmitGuessForJudgement {
        secret: Secret,
        guess: String,
    },
    /// Judge that the guess was correct!
    /// to judge it was incorrect you need to provide a counterexample.
    JudgeGuessCorrect {
        secret: Secret,
    },
    /// Judge that the guess was wrong
    JudgeGuessWrong {
        secret: Secret,
        field: Field,
        reason: ReasonGuessWrong,
    },
    /// When the "Winner" screen is shown, you can volunteer to be the next master.
    VolunteerForMaster {
        secret: Secret,
    },
}

impl GameCommand {
    /// Checks whether the inputs are clean - not likely to be maliciously formed, or erroneously
    /// entered
    pub fn is_clean(&self) -> bool {
        match self {
            // Only secret
            Self::Skip { secret }
            | Self::ClassifyField { secret, buddha: _ }
            | Self::JudgeGuessCorrect { secret }
            | Self::VolunteerForMaster { secret } => Self::check_secret(secret),
            // Secret + username
            Self::AddUser { secret, username } => {
                Self::check_secret(secret) && Self::check_username(username)
            }
            // Secret + field
            Self::SubmitFieldAsMaster {
                secret,
                field,
                buddha: _,
            }
            | Self::SubmitFieldForJudgement { secret, field }
            | Self::SubmitFieldForMondo { secret, field }
            | Self::JudgeGuessWrong {
                secret,
                field,
                reason: _,
            } => Self::check_secret(secret) && Self::check_field(field),
            // Secret + guess/rule
            Self::SubmitGuessForJudgement { secret, guess }
            | Self::SubmitRule {
                secret,
                rule: guess,
            } => Self::check_secret(secret) && Self::check_guess(guess),
        }
    }

    /// Checks whether secret confirms to requirements
    /// secret should be a SHA256 hash
    /// AKA, 64 characters long, only 0-9, a-f
    pub fn check_secret(secret: &str) -> bool {
        // Length should be correct
        if secret.len() != 64 {
            return false;
        }
        // All the characters should be lower case hex
        secret.chars().all(|c| matches!(c, 'a'..='f' | '0'..='9'))
    }

    /// Checks whether guess confirms to requirements
    fn check_guess(guess: &str) -> bool {
        guess.len() < MAX_GUESS_LEN && !guess.is_empty()
    }

    /// Checks whether field confirms to requirements
    fn check_field(field: &str) -> bool {
        if field.len() != 25 {
            return false;
        }
        field
            .chars()
            .all(|c| matches!(c, 'a'..='z' | '0'..='9' | 'A' ..='D'))
    }

    /// Checks whether username fits requirements
    /// Specifically length
    fn check_username(username: &str) -> bool {
        // TODO: check for unwanted character (emoticons that look like game elements, only spaces
        // etc.)
        // First we check the username, and make it work
        username.len() < MAX_USERNAME_LEN && !username.is_empty()
    }
}

#[derive(Serialize, Deserialize, Clone, Copy)]
pub enum ReasonGuessWrong {
    Buddha,    // The given field has the Buddha nature, but according to the guess it should not.
    NotBuddha, // The given field does not have the Buddha nature, but according to the guess it should.
    Undecidable, // The guessed rule cannot be used to decide if this is right or wrong
}

#[cfg(test)]
mod tests {
    use super::*;
    fn make_secret() -> String {
        "1234567890abcdef1234567890abcdef1234567890abcdef1234567890abcdef".to_string()
    }

    #[test]
    fn test_secret_check() {
        let c = GameCommand::Skip {
            secret: make_secret(),
        };
        assert!(c.is_clean());

        // Not allowed character
        let c = GameCommand::Skip {
            secret: "1234567890abcdefg234567890abcdef1234567890abcdef1234567890abcdef".to_string(),
        }; // Bad character .........^
        assert!(c.is_clean() == false); // g is not a valid character

        // Hash too long
        let c = GameCommand::Skip {
            secret: "1234567890abcdef1234567890abcdef1234567890abcdef1234567890abcdef0".to_string(),
        };
        assert!(c.is_clean() == false);

        // Hash too short
        let c = GameCommand::Skip {
            secret: "1234567890abcdef1234567890abcdef1234567890abcdef1234567890abcde".to_string(),
        };
        assert!(c.is_clean() == false);
    }

    #[test]
    fn test_field_check() {
        let c = GameCommand::SubmitFieldForJudgement {
            secret: make_secret(),
            field: "1234567890abcdefghijklmno".to_string(),
        };
        assert!(c.is_clean());

        // Test the other legal characters too
        let c = GameCommand::SubmitFieldForJudgement {
            secret: make_secret(),
            field: "pqrstuvwxyzABCD1234567890".to_string(),
        };
        assert!(c.is_clean());

        // Bad character
        let c = GameCommand::SubmitFieldForJudgement {
            secret: make_secret(),
            field: "12345678E0abcdefghijklmno".to_string(),
        }; // Bad character ^
        assert!(c.is_clean() == false);

        // Too short
        let c = GameCommand::SubmitFieldForJudgement {
            secret: make_secret(),
            field: "1234567890abcdefghijklmn".to_string(),
        };
        assert!(c.is_clean() == false);

        // Too long
        let c = GameCommand::SubmitFieldForJudgement {
            secret: make_secret(),
            field: "1234567890abcdefghijklmnop".to_string(),
        };
        assert!(c.is_clean() == false);
    }

    #[test]
    fn test_guess_check() {
        let c = GameCommand::SubmitGuessForJudgement {
            secret: make_secret(),
            guess: "It's red".to_string(),
        };
        assert!(c.is_clean());

        // Guess too long
        let mut longstring = String::with_capacity(4000);
        for _ in 0..3999 {
            longstring.push('.');
        }
        let c = GameCommand::SubmitGuessForJudgement {
            secret: make_secret(),
            guess: longstring,
        };
        assert!(c.is_clean() == false);

        // Empty guess
        let c = GameCommand::SubmitGuessForJudgement {
            secret: make_secret(),
            guess: String::default(),
        };
        assert!(c.is_clean() == false);
    }

    #[test]
    fn test_username_check() {
        let c = GameCommand::AddUser {
            secret: make_secret(),
            username: "Player1".to_string(),
        };
        assert!(c.is_clean());

        // Too long
        let c = GameCommand::AddUser {
            secret: make_secret(),
            username: "Player1**************************************************".to_string(),
        };
        assert!(c.is_clean() == false);

        // Empty
        let c = GameCommand::AddUser {
            secret: make_secret(),
            username: String::default(),
        };
        assert!(c.is_clean() == false);
    }
}
