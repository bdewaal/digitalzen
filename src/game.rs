use crate::command::{GameCommand, ReasonGuessWrong};
use crate::gamestage::{GameStage, PublicGameStage};
use crate::user::{PublicUser, Secret, User, UserCollection};
use crate::wrong_guess::{PublicWrongGuess, WrongGuess};
use html_escape::encode_safe;
use std::time::Instant;

pub type Field = String; // We store fields as strings
pub type CommandResult = Result<(), &'static str>;

pub const ROUNDTIME: i32 = 30; // Seconds in a round

pub struct GameState {
    users: Vec<User>,
    buddha: Vec<Field>,
    notbuddha: Vec<Field>,
    wrong_guesses: Vec<WrongGuess>,
    game_stage: GameStage,
    rule: Option<String>,
    game_id: u32,
}

impl GameState {
    pub fn new() -> Self {
        Self {
            users: Vec::new(),
            buddha: Vec::new(),
            notbuddha: Vec::new(),
            wrong_guesses: Vec::new(),
            game_stage: GameStage::new(),
            rule: None,
            game_id: 0,
        }
    }

    /// Initialize a new round
    /// Should be called with at least one volunteer in the `volunteers`
    fn new_round(&mut self, volunteers: Vec<Secret>) {
        // Setup next master
        // First, setup the initiative (so the last player to be master is first in line)
        let next_initiative = self.users.next_master_initiative();
        self.users.iter_mut().for_each(|u| {
            if u.master {
                u.master_initiative = next_initiative;
                u.master = false; // And clear everyones master status
            }
        });

        if let Some(user) = self
            .users
            .iter_mut()
            .filter(|u| volunteers.iter().any(|v| v == &u.secret))
            .min_by_key(|u| u.master_initiative)
        {
            // If there is no volunteer, the admin will have to fix using the admin screen
            user.master = true;
        }

        self.users.iter_mut().for_each(|u| u.guess_tokens = 0);
        self.buddha.clear();
        self.notbuddha.clear();
        self.wrong_guesses.clear();
        self.game_id += 1;
        self.rule = None;
        self.game_stage = GameStage::new();
    }

    fn public(&self, secret: &str) -> PublicGameState {
        PublicGameState {
            users: self.users.iter().map(|u| u.public(secret)).collect(),
            buddha: self.buddha.clone(),
            notbuddha: self.notbuddha.clone(),
            wrong_guesses: self
                .wrong_guesses
                .iter()
                .map(|g| g.public(&self.users))
                .collect(),
            game_stage: self.game_stage.public(&self.users),
            game_id: self.game_id,
            rule: {
                // We only send the rule to the player if they are master, or if the game is over
                if self.users.is_master(secret)
                    || matches!(self.game_stage, GameStage::StudentWon(_, _, _, _))
                {
                    self.rule.as_ref().map(|r| encode_safe(r).to_string())
                } else {
                    None
                }
            },
        }
    }

    /// Update game state, make sure we are up to date
    pub fn update(&mut self) {
        match self.game_stage.clone() {
            GameStage::MasterBuildingExamples =>
            // If the master has built both examples
            {
                if !(self.buddha.is_empty() || self.notbuddha.is_empty()) {
                    self.set_next_user("");
                }
            }
            GameStage::StudentBuildsField(secret, i) => {
                // If time has run out
                if i.elapsed().as_secs() >= ROUNDTIME as u64 {
                    // User goes to the end of the line
                    let next_initiative = self.users.next_initiative();
                    if let Some(user) = self.users.find_user_mut(&secret) {
                        user.initiative = next_initiative;
                    }

                    self.game_stage = GameStage::AnyStudentBuildsField;
                }
            }
            GameStage::MondoVoting(s, f, votes, i) => {
                let all_voted = self
                    .users
                    .iter()
                    .all(|u| u.master || votes.iter().any(|v| v.0 == u.secret));
                // If everyone voted or if time has run out
                if all_voted || i.elapsed().as_secs() >= ROUNDTIME as u64 {
                    self.game_stage = GameStage::MasterJudgesMondo(s, f, votes);
                }
            }
            GameStage::StudentGuesses(s, i) => {
                if self.users.find_user(&s).map_or(0, |u| u.guess_tokens) == 0
                    || i.elapsed().as_secs() >= ROUNDTIME as u64
                {
                    self.set_next_user(&s);
                }
            }
            GameStage::StudentWon(_, i, _, vu) => {
                // If time has run out, and there is a new master available
                if i.elapsed().as_secs() >= ROUNDTIME as u64 && !vu.is_empty() {
                    self.new_round(vu);
                }
            }
            _ => {}
        }
        // Set turns
        match &self.game_stage {
            GameStage::MasterMakesRule
            | GameStage::MasterBuildingExamples
            | GameStage::MasterJudges(_, _)
            | GameStage::MasterJudgesMondo(_, _, _)
            | GameStage::MasterJudgesGuess(_, _) => {
                self.users.iter_mut().for_each(|u| u.turn = u.master);
            }
            GameStage::StudentBuildsField(s, _) | GameStage::StudentGuesses(s, _) => {
                self.users.iter_mut().for_each(|u| u.turn = &u.secret == s);
            }
            GameStage::AnyStudentBuildsField => {
                self.users.iter_mut().for_each(|u| u.turn = !u.master);
            }
            GameStage::StudentWon(_, _, _, vu) => {
                // In this mode we show volunteers, instead of whose turn it is
                self.users
                    .iter_mut()
                    .for_each(|u| u.turn = vu.iter().any(|volunteer| volunteer == &u.secret));
            }
            GameStage::MondoVoting(_, _, guesses, _) => {
                self.users.iter_mut().for_each(|u| {
                    // If you're the master, or you already voted, it's not your turn
                    u.turn = !(u.master || guesses.iter().any(|s| s.0 == u.secret))
                });
            }
        }
        self.users.order();
    }

    /// Receive a command from the main command page
    pub fn receive_command(&mut self, command: GameCommand) -> CommandResult {
        match command {
            GameCommand::AddUser { secret, username } => {
                if self.users.add_user(secret, username) {
                    Ok(())
                } else {
                    Err("Could not add user")
                }
            }
            GameCommand::SubmitRule { secret, rule } => {
                // Must be right stage
                if !matches!(self.game_stage, GameStage::MasterMakesRule) {
                    return Err("May only submit new rule once, at start of game");
                }
                // Must be master
                if !self.users.is_master(&secret) {
                    return Err("Only Master may submit rule");
                }
                self.game_stage = GameStage::MasterBuildingExamples;
                self.rule = Some(rule);
                Ok(())
            }
            // Always allowed, so we just accept
            GameCommand::SubmitFieldAsMaster {
                secret,
                field,
                buddha,
            } => {
                if self.users.is_master(&secret) {
                    self.add_field(&field, buddha);
                    Ok(())
                } else {
                    Err("Only Master may submit field")
                }
            }
            GameCommand::Skip { secret } => self.skip(&secret),
            GameCommand::SubmitFieldForMondo { secret, field } => {
                self.submit_field_for_mondo(secret, field)
            }
            GameCommand::SubmitFieldForJudgement { secret, field } => {
                self.submit_field_for_judgement(secret, field)
            }
            GameCommand::ClassifyField { secret, buddha } => self.classify_field(secret, buddha),
            GameCommand::SubmitGuessForJudgement { secret, guess } => {
                self.submit_guess(secret, guess)
            }
            GameCommand::JudgeGuessCorrect { secret } => self.judge_guess_correct(secret),
            GameCommand::JudgeGuessWrong {
                secret,
                field,
                reason,
            } => self.judge_guess_wrong(secret, field, reason),
            GameCommand::VolunteerForMaster { secret } => {
                if !self.users.exists(&secret) {
                    return Err("User not found");
                }
                if let GameStage::StudentWon(_, _, _, vu) = &mut self.game_stage {
                    if !vu.iter().any(|u| u == &secret) {
                        vu.push(secret);
                    }
                    Ok(())
                } else {
                    Err("You may not volunteer now")
                }
            }
        }
    }

    /// Skip either a guess or building a field
    fn skip(&mut self, secret: &str) -> CommandResult {
        match self.game_stage.clone() {
            GameStage::StudentGuesses(s, _) => {
                if s == secret {
                    self.set_next_user(&s);
                    Ok(())
                } else {
                    Err("Not your turn to skip")
                }
            }
            GameStage::StudentBuildsField(s, _) => {
                if s == secret {
                    self.game_stage = GameStage::AnyStudentBuildsField;
                    Ok(())
                } else {
                    Err("Not your turn to skip")
                }
            }
            _ => Err("Not possible to skip right now"),
        }
    }

    /// Output the "public" JSON representation of the game state
    /// Note that this is where we lazily update the game state
    pub fn to_json(&self, secret: &str) -> String {
        serde_json::to_string(&self.public(secret)).unwrap_or_else(|_| String::from("{}"))
    }

    pub fn admin_json(&self) -> String {
        serde_json::to_string(&self.users).unwrap_or_else(|_| String::from("{}"))
    }

    fn field_exists(&self, field: &str) -> bool {
        self.buddha
            .iter()
            .chain(self.notbuddha.iter())
            .any(|f| f == field)
    }

    fn add_field(&mut self, field: &str, buddha: bool) {
        // Remove any previous instances of this hash
        self.buddha.retain(|f| f != field);
        self.notbuddha.retain(|f| f != field);

        if buddha {
            self.buddha.push(field.to_string());
        } else {
            self.notbuddha.push(field.to_string());
        }
    }

    fn submit_field_for_judgement(&mut self, secret: String, field: String) -> CommandResult {
        if self.users.exists(&secret) {
            // Don't allow submitting fields that already exist
            if self.field_exists(&field) {
                return Err("Field already exists");
            }

            self.update();
            match &self.game_stage {
                GameStage::StudentBuildsField(s, _) => {
                    if &secret == s {
                        self.game_stage = GameStage::MasterJudges(secret, field);
                        Ok(())
                    } else {
                        Err("Only one student may submit field right now")
                    }
                }
                GameStage::AnyStudentBuildsField => {
                    self.game_stage = GameStage::MasterJudges(secret, field);
                    Ok(())
                }
                _ => Err("Not possible to submit field right now"),
            }
        } else {
            Err("User not found, please log in")
        }
    }

    /// Judge a guess the user made is correct, so the player wins!
    /// If the judgement would be "Incorrect", the master should give a counterexample
    fn judge_guess_correct(&mut self, secret: Secret) -> CommandResult {
        if !self.users.is_master(&secret) {
            return Err("Only master may judge");
        }
        match self.game_stage.clone() {
            GameStage::MasterJudgesGuess(s, g) => {
                // Add to the win score
                self.users
                    .find_user_mut(&s)
                    .iter_mut()
                    .for_each(|u| u.wins += 1);
                // And get ready for the next stage
                self.game_stage = GameStage::StudentWon(s, Instant::now(), g, Vec::new());
                Ok(())
            }
            _ => Err("May not judge guess right now"),
        }
    }

    /// Judge a guess the user made is incorrect, including the reason
    /// The reason is always a counterexample, so it's included
    fn judge_guess_wrong(
        &mut self,
        secret: Secret,
        field: Field,
        reason: ReasonGuessWrong,
    ) -> CommandResult {
        // Must be master to judge
        if !self.users.is_master(&secret) {
            return Err("Only master may judge");
        }
        match self.game_stage.clone() {
            GameStage::MasterJudgesGuess(s, g) => {
                // Take away the guess token from the user
                // and set the next stage in the game
                if let Some(user) = self.users.find_user_mut(&s) {
                    user.guess_tokens -= 1;
                    if user.guess_tokens > 0 {
                        self.game_stage = GameStage::StudentGuesses(s, Instant::now());
                    } else {
                        // No guesses left, so next user goes
                        self.set_next_user(&s);
                    }
                } else {
                    // Shouldn't happen, unless we delete the user at a weird time
                    self.set_next_user(&s);
                }

                // If the reason wasn't "unclear", the counterexample should be added to one of the
                // two columns
                match reason {
                    ReasonGuessWrong::Buddha => self.add_field(&field, true),
                    ReasonGuessWrong::NotBuddha => self.add_field(&field, false),
                    ReasonGuessWrong::Undecidable => (),
                }

                // Add to the list of wrong guesses
                self.wrong_guesses
                    .push(WrongGuess::new(secret, field, g, reason));
                Ok(())
            }
            _ => Err("May not judge right now"),
        }
    }

    /// Submit a guess, so the user can win the game if they have it right
    ///
    /// Secret is the unique identifier for the user submitting
    /// Guess is the text of the guess made
    fn submit_guess(&mut self, secret: Secret, guess: String) -> CommandResult {
        match self.game_stage.clone() {
            GameStage::StudentGuesses(s, _) => {
                if s == secret && self.users.find_user(&secret).map_or(0, |u| u.guess_tokens) > 0 {
                    self.game_stage = GameStage::MasterJudgesGuess(secret, guess);
                    Ok(())
                } else {
                    Err("Not your turn to submit guess")
                }
            }
            _ => Err("Not the time to submit guess"),
        }
    }

    /// Submit a field, and specify it should be voted on as a mondo
    ///
    /// secret is the unique identifier for the user submitting
    /// field is the field to be judged
    fn submit_field_for_mondo(&mut self, secret: String, field: String) -> CommandResult {
        if self.users.exists(&secret) {
            // Don't allow submitting fields that already exist
            if self.field_exists(&field) {
                return Err("Field already exists");
            }

            self.update();
            match &self.game_stage {
                GameStage::StudentBuildsField(s, _) => {
                    if &secret == s {
                        self.game_stage =
                            GameStage::MondoVoting(secret, field, Vec::new(), Instant::now());
                        Ok(())
                    } else {
                        Err("Not your turn to enter field")
                    }
                }
                GameStage::AnyStudentBuildsField => {
                    self.game_stage =
                        GameStage::MondoVoting(secret, field, Vec::new(), Instant::now());
                    Ok(())
                }
                _ => Err("Not possible to enter field right now"),
            }
        } else {
            Err("User does not exist")
        }
    }

    /// Specify the Buddha nature of the field that is up for classification (the one in the
    /// `GameStage`)
    /// Depending on the stage this could be done by either master or student
    fn classify_field(&mut self, secret: String, buddha: bool) -> CommandResult {
        if self.users.exists(&secret) {
            match self.game_stage.clone() {
                GameStage::MondoVoting(s, f, mut votes, i) => {
                    // In the MondoVoting stage, enter a guess ("vote") as to whether the field has
                    // the Buddha nature or not.

                    // Masters can't vote
                    if self.users.is_master(&secret) {
                        return Err("Masters may not vote on mondo");
                    }

                    // If the user already voted, find and change that vote
                    let overwrite = votes.iter_mut().find(|u| u.0 == secret).map_or(false, |t| {
                        t.1 = buddha; // change the vote
                        true // then return that you found it
                    });
                    // Otherwise, just add a new classification
                    if !overwrite {
                        votes.push((secret, buddha));
                    }
                    self.game_stage = GameStage::MondoVoting(s, f, votes, i);
                    Ok(())
                }
                GameStage::MasterJudges(s, f) => {
                    // Only the master may judge
                    if !self.users.is_master(&secret) {
                        return Err("Only the master may judge now");
                    }

                    self.add_field(&f, buddha);
                    self.game_stage = GameStage::StudentGuesses(s, Instant::now());
                    Ok(())
                }
                GameStage::MasterJudgesMondo(s, f, guesses) => {
                    // Only the master may judge
                    if !self.users.is_master(&secret) {
                        return Err("Only the master may judge now");
                    }

                    self.add_field(&f, buddha);
                    guesses.iter().for_each(|u| {
                        if u.1 == buddha {
                            // if they guessed correctly
                            self.users
                                .find_user_mut(&u.0)
                                .iter_mut()
                                .for_each(|user| user.guess_tokens += 1);
                        }
                    });
                    self.game_stage = GameStage::StudentGuesses(s, Instant::now());
                    Ok(())
                }
                _ => Err("Not possible to judge right now"),
            }
        } else {
            Err("User not found, please log in")
        }
    }

    /// Set the next user in the `StudentBuildsField` state
    /// The secret is used to update the initiative
    /// Note that if we can't find a user under secret, it ignores it
    /// So if there is no previous user, just pass in an empty string
    fn set_next_user(&mut self, secret: &str) {
        // Update initiative
        let next_initiative = self.users.next_initiative();
        if let Some(user) = self.users.find_user_mut(&secret) {
            user.initiative = next_initiative;
        }
        // Set next student
        if let Some(next_student) = self.users.next_user() {
            self.game_stage = GameStage::StudentBuildsField(next_student, Instant::now());
        } else {
            self.game_stage = GameStage::AnyStudentBuildsField;
        }
    }

    pub fn make_user_master(&mut self, secret: &str) {
        self.users
            .iter_mut()
            .for_each(|u| u.master = u.secret == secret);
    }

    pub fn kick_user(&mut self, secret: &str) {
        self.users.retain(|u| u.secret != secret);
    }

    pub fn is_user(&self, secret: &str) -> bool {
        self.users.exists(secret)
    }
}

#[derive(Serialize)]
struct PublicGameState {
    users: Vec<PublicUser>,
    buddha: Vec<Field>,
    notbuddha: Vec<Field>,
    wrong_guesses: Vec<PublicWrongGuess>,
    game_stage: PublicGameStage,
    game_id: u32,
    rule: Option<String>,
}
