// Colors used for drawing cell
var colors = Array(
    "rgb(230, 230, 230)",
    "rgb(235, 20, 20)",
    "rgb(30, 235, 20)",
    "rgb(30, 15, 225)",
);

var SHAPES_COUNT = 4;


// Cell = one of the cells that can be clicked on etc.
class Cell {
    constructor(dim) {
        this.color1 = 0;
        this.color2 = 1;
        this.shape=0;
        this.dim = dim; // Dimension of each cell
        this.hasflipped = false;
    }
    draw(ctx, x,y) {
        ctx.fillStyle = "rgba(255, 255, 255, 1)";
        var dim = this.dim;
        var basex = x*dim;
        var basey = y*dim;
        ctx.fillRect(basex, basey, dim, dim);
        if (this.shape==1) {
            this.setcolor(ctx, this.color1);
            ctx.fillRect(1+basex, 1+basey, dim-2, dim-2);
            this.setcolor(ctx, this.color2);
            ctx.beginPath();
            ctx.moveTo(basex+1,basey+2);
            ctx.lineTo(basex+1, basey+dim-1);
            ctx.lineTo(basex+dim-1, basey+dim-1);
            ctx.closePath();
            ctx.fill();
        } else if (this.shape==2) {
            this.setcolor(ctx, this.color1);
            ctx.fillRect(1+basex, 1+basey, dim-2, dim-2);
            this.setcolor(ctx, this.color2);
            ctx.beginPath();
            ctx.moveTo(basex+1,basey+1);
            ctx.lineTo(basex+dim-1, basey+1);
            ctx.lineTo(basex+1, basey+dim-1);
            ctx.closePath();
            ctx.fill();
        } else if (this.shape==3) {
            this.setcolor(ctx, this.color1);
            ctx.fillRect(1+basex, 1+basey, dim-2, dim-2);
            this.setcolor(ctx, this.color2);
            ctx.beginPath();
            ctx.arc(basex + dim/2, basey+dim/2, dim/3, 0, 2* Math.PI);
            ctx.closePath();
            ctx.fill();
        } else {
            this.setcolor(ctx, this.color1);
            ctx.fillRect(1+basex, 1+basey, dim-2, dim-2);
        }
    }
    setcolor(ctx, color) {
        ctx.fillStyle = colors[color];
    }

    colornext(x,y) {
        var h = this.dim/2;
        if (this.shape == 1) {
            if (x < y) {
                this.color2next();
            } else {
                this.color1next();
            }
        } else if (this.shape == 2) {
            if (x + y < this.dim ) {
                this.color2next();
            } else {
                this.color1next();
            }
        } else if (this.shape == 3) {
            if ((x-h)*(x-h)+(y-h)*(y-h) < this.dim*this.dim/3/3 ) {
                this.color2next();
            } else {
                this.color1next();
            }
        } else {
            this.color1 = (this.color1 +1) % colors.length;
            if (this.color1 == this.color2) {
                this.color2next();
            }
        }
    }
    color1next() {
        this.color1 = (this.color1 +1) % colors.length;
        if (this.color1 == this.color2) {
            this.color1next();
        }
    }
    color2next() {
        this.color2 = (this.color2 +1) % colors.length;
        if (this.color1 == this.color2) {
            this.color2next();
        }
    }
    // Select the next shape
    shapenext() {
        // If the shape is 0, make sure the colors are different for the next one
        if (this.shape == 0 && this.color1 == this.color2) {
            this.color2 = (this.color1+1) %colors.length;
        }
        if (this.shape == 0) {
            this.hasflipped = false;
        }
        if (this.shape == 2) {
            var t = this.color1;
            this.color1 = this.color2;
            this.color2 = t;
            if (this.hasflipped == false) {
                this.shape = 1;
                this.hasflipped = true;
            } else {
                this.hasflipped = false;
                this.shape= 3;
            }
        } else {
            this.shape = (this.shape + 1) % SHAPES_COUNT;
        }
    }

    hash() {
        let n;
        // Work out the n
        if (this.shape == 0) {
            n = this.color1;
        } else {
            // encode non-0 shape
            n = colors.length;
            console.log(n);
            // encode specific shape
            let shape_len = colors.length*(colors.length-1);
            n += shape_len*(this.shape-1);
            console.log(n);
            // encode color1
            n += (colors.length-1) * this.color1;
            console.log(n);
            // encode color2
            if (this.color2 >= this.color1) {
                n += this.color2 - 1;
                console.log(n);
            } else {
                n+= this.color2;
                console.log(n);
            }
        }
        // Encode character
        if (n <= 9) {
            return String.fromCodePoint(48+n);
        } else if (n <= 35) {
            return String.fromCodePoint(97+n-10);
        } else if (n <= 65) {
            return String.fromCodePoint(65+n-36);
        } else {
            return "0";
        }
    }

    fromhash(hashval) {
        let cp = hashval.codePointAt(0);
        let n = 0; // If in doubt, just make it 0
        if (48 <= cp && cp <= 57) {
            // '0' through '9'
            // map to 0 to 9
            n = cp-48;
        } else if (97 <= cp && cp <= 122) {
            // 'a' through 'z'
            // map to 10 to 35
            n = 10 + cp - 97;
        } else if (65 <= cp && cp <= 90) {
            // 'A' through 'Z'
            // map to 36 to 61
            n = 36 + cp - 65;
        }

        // Special case for solid colors
        if (n < colors.length) {
            this.shape = 0;
            this.color1 = n;
            this.color2 = (n +1) % colors.length;
            return;
        } 
        // Renumber for easier math
        n = n-colors.length;

        // Ever shape has x(x-1) possibities: x colors for color1, and one less for color2
        let shape_len = colors.length*(colors.length-1);
        this.shape = (Math.floor(n / (shape_len)) +1 ) % SHAPES_COUNT;

        // Renumber to remove shape, for easier math
        n = n % shape_len;
        this.color1 = Math.floor(n / (colors.length - 1));

        // Renumber for final color sequence
        n = n % (colors.length - 1);
        if (n >= this.color1) {
            n += 1;
        }
        this.color2 = n;
    }
}

class Field {
    constructor(canvas_name, hash, input_name, scale=false) {
        // If input_name is None, this is not supposed to be interactive
        this.state = Array(5);
        this.dim = Math.floor(Math.min(
            $("#"+canvas_name).height(),
            $("#"+canvas_name).width())/5);
        if (scale) {
            this.dim = this.dim - 3;
        }
        for (let x=0; x < 5;x++) {
            this.state[x] = Array(5);
            for (let y=0; y< 5;y++) {
                this.state[x][y] = new Cell(this.dim);
            }
        }
        this.fromhash(hash);

        // Setup the canvas
        this.canvas = document.getElementById(canvas_name);
        this.ctx = this.canvas.getContext("2d");


        if (input_name !== null) {
            $("#" + canvas_name).contextmenu(function(event) {event.preventDefault(); return false;});

            $("#" + canvas_name).on("mousedown", {field: this}, function(event) {
                var dim = event.data.field.dim;
                var px_x = event.pageX - $(this).offset().left;
                var px_y = event.pageY - $(this).offset().top;
                var x = Math.floor(px_x / dim);
                var y = Math.floor(px_y / dim);

                if (x >= event.data.field.state.length  ||
                    y >= event.data.field.state.length) {
                    return;
                }
                if (event.button == 0) {
                    event.data.field.state[x][y].colornext(px_x%dim,px_y%dim);
                }
                if (event.button == 2) {
                    event.data.field.state[x][y].shapenext();
                }
                event.data.field.draw();
            });

            this.input_name = input_name;
            this.writehash();
            $("#"+input_name).blur({field: this}, function(event) {
                var hash = $(this).val();
                event.data.field.fromhash(hash);
                event.data.field.draw();
            });
        }
        if (scale) {
            this.ctx.fillstyle = "black";
            this.ctx.font = "12px sans";
            this.textBaseLine = "middle";
            this.ctx.fillText("5", this.dim * 5 + 5, this.dim/2);
            this.ctx.fillText("4", this.dim * 5 + 5, this.dim * 1.5);
            this.ctx.fillText("3", this.dim * 5 + 5, this.dim * 2.5);
            this.ctx.fillText("2", this.dim * 5 + 5, this.dim * 3.5);
            this.ctx.fillText("1", this.dim * 5 + 5, this.dim * 4.5);
            this.ctx.fillText("a", this.dim*0.5, this.dim * 5 + 11);
            this.ctx.fillText("b", this.dim*1.5, this.dim * 5 + 11);
            this.ctx.fillText("c", this.dim*2.5, this.dim * 5 + 11);
            this.ctx.fillText("d", this.dim*3.5, this.dim * 5 + 11);
            this.ctx.fillText("e", this.dim*4.5, this.dim * 5 + 11);
        }

    }
    draw() {
        for (let x=0; x < 5;x++) {
            for (let y=0; y< 5;y++) {
                this.state[x][y].draw(this.ctx, x,y);
            }
        }
        this.writehash();
    }
    hash () {
        var output = "";
        for (let x=0; x < 5;x++) {
            for (let y=0; y< 5;y++) {
                output += this.state[x][y].hash();
            }
        }
        return output;
    }
    writehash () {
        $("#" + this.input_name).val(this.hash());
    }
    fromhash(hash) {
        hash = hash.trim();
        if (hash.length < 25) {
            hash = hash + "0000000000000000000000000";
        }
        for (let x=0; x < 5;x++) {
            for (let y=0; y< 5;y++) {
                var char1 = hash[(5*x+y)];
                this.state[x][y].fromhash(char1);
            }
        }
    }
    reset() {
        this.fromhash("0");
    }
}

var id_holder = 0;
function get_id() {
    id_holder += 1;
    return id_holder;
}

// Add the canvas with the field to the target container
// target should be specified without the #
function add_example(target, hash, mainfield) {
    var id = get_id();
    $("#"+target).append("<canvas width=100 height=100 id='canvas" + id + "'></canvas><br>");
    if (mainfield) {
        $("#canvas"+id).on("click", {hashval: hash, mainfield: mainfield}, function(event){
            event.data.mainfield.fromhash(event.data.hashval);
            event.data.mainfield.draw();
        });
    }
    var nf = new Field("canvas"+id,hash, null);
    nf.draw();
}
