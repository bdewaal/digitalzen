// DisplayUpdate, used to only update the display when required
class DisplayUpdate {
    // Master: whether current player is the master
    // Seconds left: can be either a number or null
    constructor(master, my_turn, stage, rule) {
        this.rule = rule;
        this.html = ""; // The HTML to display in the "statescreen" div
        this.field = ""; // The field to display in the statescreen div,
        // if any. If anything is here, there should be
        // a canvas in the html.
        this.master = master; // boolean, whether current player is master right now
        this.header_text = ""; // To display next to the countdown
        this.title_text = "DigitalZen"; // Title of the page
        this.seconds_left = null; // Can be either null or a number 
        this.skip_turn_disabled = true; //bool
        this.unclear_button_hidden = true; //bool
        this.my_turn = my_turn; //bool
        this.stage = stage; // Text of the stage

        this.finishing_function = function() {};

        this.submit_function = function() {};
        this.submit_disabled = true; //bool

        this.post_field_disabled = true; //bool
        this.post_button1_function = function() {};
        this.post_button2_function = function() {};

        this.alert_flash_state = true; // Used to flash the "!" in the title on and off
    }

    update(last_state) {
        if (last_state.rule != this.rule) {
            // If the rule has been submitted, we want to lock it in
            if (this.master && this.rule != null) {
                $("#enterguess").val(this.rule).prop("disabled", true);
            } else {
                $("#enterguess").prop("disabled", false);
            }
        }
        if (last_state.html != this.html) {
            $("#statescreen").html(this.html);
            if (this.field.length > 0) {
                var nf = new Field("status_canvas", this.field, null);
                nf.draw();
            }
        }
        if (last_state.header_text != this.header_text) {
            $("#status-text").html(this.header_text);
        }
        if (last_state.master != this.master) {
            if (this.master) {
                $("#game").addClass("master");
                $("#gamebutton1").html("Buddha");
                $("#gamebutton2").html("Not Buddha");
            } else {
                $("#game").removeClass("master");
                $("#gamebutton1").html("Judge");
                $("#gamebutton2").html("Mondo");
            }
        }
        if (last_state.post_field_disabled != this.post_field_disabled) {
            $("#gamebutton1").prop("disabled", this.post_field_disabled);
            $("#gamebutton2").prop("disabled", this.post_field_disabled);
        }
        if (last_state.submit_disabled != this.submit_disabled) {
            $("#submitguess").prop("disabled", this.submit_disabled);
        }
        if (last_state.skip_turn_disabled != this.skip_turn_disabled) {
            $("#skip").prop("disabled", this.skip_turn_disabled);
        }
        if (last_state.unclear_button_hidden != this.unclear_button_hidden) {
            if (this.unclear_button_hidden) {
                $("#reject-unclear").addClass("hidden");
            } else {
                $("#reject-unclear").removeClass("hidden");
            }
        }
        if (last_state.my_turn != this.my_turn || last_state.seconds_left != this.seconds_left) {
            if (this.my_turn) {
                $("#countdown").addClass("alert");
                if (this.seconds_left == null) {
                    $("#countdown").html("!");
                } else {
                    $("#countdown").html(this.seconds_left);
                }
                changeFavicon("/static/favicon-alert.ico");
            } else { // Not my turn
                $("#countdown").removeClass("alert");
                if (this.seconds_left == null) {
                    $("#countdown").html("-");
                } else {
                    $("#countdown").html(this.seconds_left);
                }
                changeFavicon("/favicon.ico");
            }
        }
        // Set page title
        if (last_state.title_text != this.title_text || this.my_turn) {
            this.alert_flash_state = !last_state.alert_flash_state; // Change this every time
            // If it's my turn, flash the exclamation mark
            if (this.my_turn && this.alert_flash_state) {
                document.title = "Your turn";
            } else {
                document.title = this.title_text;
            }
        }
        if (last_state.stage != this.stage) {
            $("#submitguess").off("click").click(this.submit_function);
            $("#gamebutton1").off("click").click(this.post_button1_function);
            $("#gamebutton2").off("click").click(this.post_button2_function);
        }
        this.finishing_function();
    }
}

// Holder for game state
class Game {
    constructor() {
        this.salt = null;
        this.me_master = null;
        this.my_turn = null;
        this.buddha_hashes = Array();
        this.not_buddha_hashes = Array();
        this.wrong_guesses = Array();
        // Give the previous update nonsense values, so it has to update the first time
        this.previous_update = new DisplayUpdate(null, null, null, -1);
        this.previous_update.post_field_disabled = null;
        this.previous_update.submit_disabled = null;
        this.previous_update.skip_turn_disabled = null;
        this.previous_update.seconds_left = -1;
        this.previous_update.unclear_button_hidden = null;
        this.title_text = null;
    }

    password() {
        var pass_raw = $("#password").val();
        return sha256(pass_raw + this.salt);
    }

    display_users(users) {
        this.me_master = false;
        this.my_turn = false;

        var userlist = "";
        for (var i in users) {
            var u = users[i];

            if (u.me) {
                // Color "me" differently
                userlist += "<li class=\"me\">"+u.username;
            } else {
                userlist += "<li>"+u.username;
            }

            userlist += " ";
            // Display win stars
            userlist += "&#x2b50;".repeat(u.wins);
            
            // Display guess icons
            userlist += "&#x1f4a0;".repeat(u.guess_tokens);

            // Display master Icon
            if (u.master) {
                userlist += "&#x1f5ff;";
                if (u.me) {// if I am the master
                    this.me_master = true;
                }
            }
            // Display turn arrow
            if (u.turn) {
                userlist += "&#x21e6;";
                if (u.me) {// if I am the master
                    this.my_turn = true;
                }
            }
            userlist += "</li>";
        }
        $("#userlist").html(userlist);
    }

    // From list: list (typically from server) with hashes that should be
    // displayed here
    // To list: list where we store the hashes already displayed
    // column name: div id (without the "#") where to display them
    display_hashes(from_list, to_list, column_name) {
        var i;
        // Handle Buddha list
        // Is there anything there that shouldn't be?
        for (i in to_list) {
            if (!from_list.includes(to_list[i])) {
                // If there are any missing
                // Burn it all!
                to_list.length = 0; // This clears the array
                $("#" + column_name).html("");
                break;
            }
        }
        // Write in new
        for (i in from_list){
            var b = from_list[i];
            if (!to_list.includes(b)) {
                to_list.push(b);
                add_example(column_name, b, big_field);
            }
        }
    }

    // Display the previous guesses
    // server_guesses is the list from the server, which at the end our guess
    // list should match.
    display_guesses(server_guesses) {
        var i;
        // Are there any guesses in our list missing from the server list?
        // This can happen if usernames change
        for (i in this.wrong_guesses) {
            var wg = this.wrong_guesses[i];
            //if (!from_list.includes(to_list[i])) {
            if (!server_guesses.some(g => (g.guess == wg.guess) && (g.username == wg.username))) {
                // If there are any missing
                // Burn it all!
                this.wrong_guesses.length = 0; // This clears the array
                $("#wrong-guesses").html("");
                break;
            }
        }
        // Write in new
        for (i in server_guesses) {
            var wg = server_guesses[i];
            if (!this.wrong_guesses.some(g => (g.guess == wg.guess))) {
                this.wrong_guesses.push(wg);
                var newguessid = "wrongguess" + get_id();
                var newguessdiv = "<div><div id=\"" + newguessid + "\"></div><div>";
                newguessdiv += "<i>" + wg.guess + "</i><br>";
                if (wg.reason == "Buddha") {
                    newguessdiv += "<b>" + wg.username + "</b> thought that this shouldn't have the Buddha nature, but it does";
                } else if (wg.reason == "NotBuddha") {
                    newguessdiv += "<b>" + wg.username + "</b> thought that this should have the Buddha nature, but it doesn't";
                } else {
                    newguessdiv += "The master didn't know how to apply <b>" + wg.username + "</b>'s rule here";
                }
                newguessdiv += "</div>";
                $("#wrong-guesses").append(newguessdiv);
                add_example(newguessid, wg.field, big_field);
            }
        }
    }

    handle_status(stat) {
        this.display_users(stat.users);
        this.display_hashes(stat.buddha, this.buddha_hashes, "good");
        this.display_hashes(stat.notbuddha, this.not_buddha_hashes, "bad");
        this.display_guesses(stat.wrong_guesses);

        // Fill in the next 
        var next = new DisplayUpdate(this.me_master, this.my_turn, stat.game_stage, stat.rule);
        this.fill_in_update(stat.game_stage, next);

        next.update(this.previous_update);

        this.previous_update = next;
    }

    fill_in_update(game_stage, next) {
        // tags
        // Used in some of the html below, will often be undefined
        var student = game_stage.username; 
        var seconds = game_stage.remaining_time;
        var canvas = `<br><canvas id="status_canvas"></canvas><br>
                        (by student ${student})<br>`;
        var guess = game_stage.guess;
        var judge_buddha_buttons = `
        <button id="judgebuddha">Buddha</button>
        <button id="judgenotbuddha">Not Buddha</button>
        `;

        // Function used many times
        var classify_field = function() {
            $("#judgebuddha").off("click").click(function() {
                submit_command( {
                    command: "ClassifyField",
                    buddha: true
                });
            });
            $("#judgenotbuddha").off("click").click(function() {
                submit_command( {
                    command: "ClassifyField",
                    buddha: false,
                });
            });
        };


        if (game_stage.stage == "MasterMakesRule") {
            next.header_text="Master making rule";
            if (this.me_master) {
                next.html = `
                Make up a rule, and submit it below.
                <br>
                Some points about rules:
                <ul>
                <li> The rule can be positive or negative. You could have
                either "Contains a green square" or "Does not contain a green
                square".</li>
                <li> It should always be clear whether the rule applies to a
                field or not. It should never be a matter of judgement or
                opinion. So don't make a rule like "looks like a face"</li>
                <li>Keep it simple! No one minds a rule that is too easy, but a
                rule that is too hard is frustrating for everyone.</li>
                </ul>
                `;
                next.submit_disabled = false;
                next.submit_function = function() {
                    submit_command( {
                        command: "SubmitRule",
                        rule: $("#enterguess").val()
                    });
                };
            } else {
                next.html = "Waiting for the master to invent the rule";
            }
        }
        if (game_stage.stage == "MasterBuildingExamples") {
            next.header_text="Master making examples";
            if (this.me_master) {
                next.html = `
                Provide two examples of your rule:
                <br>One of a field that does match the rule (then click "Buddha"), and one that doesn't (then click "Not Buddha").
                `;
                next.post_field_disabled = false;
                next.post_button1_function = function(){
                    submit_command({
                        command: "SubmitFieldAsMaster",
                        field: big_field.hash(),
                        buddha: true 
                    });
                };
                next.post_button2_function = function(){
                    submit_command({
                        command: "SubmitFieldAsMaster",
                        field: big_field.hash(),
                        buddha: false 
                    });
                };
            } else {
                next.html = "Waiting for the master to build the examples";
            }
        }

        if (game_stage.stage == "StudentBuildsField") {
            next.header_text=`${student} building field`;
            next.seconds_left = seconds;
            if (this.me_master) {
                next.html = `
                Wait for one of your students to come up with a field for you
                to judge. Right now ${student} may guess, but in ${seconds}s
                anyone may try.
                `;
            } else {
                if (this.my_turn) {
                    next.html = `
                    You need to build a field for the master to judge! 
                    You have ${seconds} seconds to submit one, otherwise anyone can try.
                    `;
                    next.post_field_disabled = false;
                    next.skip_turn_disabled = false;
                    next.post_button1_function = function(){
                        submit_command({
                            command: "SubmitFieldForJudgement",
                            field: big_field.hash(),
                        });
                    };
                    next.post_button2_function = function(){
                        submit_command({
                            command: "SubmitFieldForMondo",
                            field: big_field.hash(),
                        });
                    };
                } else {
                    next.html = `
                    Someone needs to build a field for the master to judge! 
                    ${student} has ${seconds} seconds to submit one, otherwise anyone can try.
                    `;
                }
            }
        }
        if (game_stage.stage == "AnyStudentBuildsField") {
            next.header_text="Build a field";
            if (this.me_master) {
                next.html = `
                Wait for one of your students to come up with a field for you
                to judge. Anyone may try.
                `;
            } else {
                next.html = `
                    Someone needs to build a field for the master to judge! 
                    Anyone may try, first to submit gets this turn.
                    `;
                next.post_field_disabled = false;
                next.post_button1_function = function(){
                    submit_command({
                        command: "SubmitFieldForJudgement",
                        field: big_field.hash(),
                    });
                };
                next.post_button2_function = function(){
                    submit_command({
                        command: "SubmitFieldForMondo",
                        field: big_field.hash(),
                    });
                };
            }
        }
        if (game_stage.stage == "MasterJudges") {
            next.header_text="Master judges field";
            next.field = game_stage.field;
            if (this.me_master) {
                next.html = `
                You need to state whether this field has the Buddha nature
                ${canvas}
                ${judge_buddha_buttons}
                `;
                next.finishing_function = classify_field;
            } else {
                next.html = `Waiting for the master to judge this ${canvas}`;
            }
        }
        if (game_stage.stage == "MondoVoting") {
            next.header_text="Students vote on Mondo";
            next.field = game_stage.field;
            next.seconds_left = seconds;
            if (this.me_master) {
                next.html = `
                Waiting for students to judge this
                ${canvas}
                `;
            } else {
                next.html = `
                ${student} submitted a Mondo!<br>
                That means everyone gets to guess whether this field has the
                Buddha nature or not.
                ${canvas}
                ${judge_buddha_buttons}
                <br>If you get it right, you get a &#x1f4a0;.
                You have ${seconds} to guess.
                `;
                next.finishing_function = classify_field;
            }
        }

        if (game_stage.stage == "MasterJudgesMondo") {
            next.header_text="Master judges mondo";
            next.field = game_stage.field;
            
            // Prepare users, so we can display them
            var users = "";
            var ug = game_stage.user_guesses;
            var i;
            for (i in ug) {
                var uname = ug[i][0];
                users += "<li>" + uname + " (";
                if (!ug[i][1]) { // If they guessed not Buddha
                    users += "Not ";
                }
                users += "Buddha) </li>";
            }

            if (this.me_master) {
                next.html = `
                You need to state whether this field has the Buddha nature
                ${canvas}
                ${judge_buddha_buttons}
                <br> It was a mondo, and these students guessed: <ul>${users}</ul>
                `;
                next.finishing_function = classify_field;
            } else {
                next.html = `Waiting for the master to judge this
                ${canvas}
                <br> It was a mondo, and these students guessed: <ul>${users}</ul>
                `;
            }
        }
        if (game_stage.stage == "StudentGuesses") {
            next.header_text=`${student} guesses rule`;
            next.seconds_left = seconds;
            if (this.my_turn) {
                next.html = `
                You may submit a guess. You have ${seconds}s.
                <br> This will cost you a &#x1f4a0;.
                <br> If you don't know, you can skip.
                `;
                next.skip_turn_disabled = false;
                next.submit_disabled = false;
                next.submit_function = function() {
                    submit_command({
                        command: "SubmitGuessForJudgement",
                        guess: $("#enterguess").val()
                    });
                };
            } else {
                next.html = `Waiting for ${student} to guess. They have ${seconds}s.`;
            }
        }
        if (game_stage.stage == "MasterJudgesGuess") {
            next.header_text=`Master judges ${student}'s rule`;
            if (this.me_master) {
                next.html = `${student} has guessed the following:
                <br>${guess}
                <br>
                <button id="rightguess">Correct!</button>
                <br>
                If it's wrong, as Master it's your job to <emph>prove</emph> it wrong.
                You have to provide a counterexample.
                Enter a field and say why it's a counterexample:
                <ul> 
                    <li>A field that does have the Buddha nature, but doesn't fit the guessed rule should be marked "Buddha"</li>
                    <li>A field that doesn't have the Buddha nature, but does fit the guessed rule should be marked "Not Buddha"</li>
                    <li>If it isn't clear if the given field fits the guessed rule, press "Unclear"</li>
                </ul>
                `;
                next.post_field_disabled = false;
                next.unclear_button_hidden = false;
                next.finishing_function = function() {
                    $("#rightguess").off("click").click(function() {
                        submit_command( {
                            command: "JudgeGuessCorrect",
                            buddha: true
                        });
                    });
                };
                next.post_button1_function = function() {
                    submit_command({
                        command: "JudgeGuessWrong",
                        field: big_field.hash(),
                        reason: "Buddha"
                    });
                };
                next.post_button2_function = function() {
                    submit_command({
                        command: "JudgeGuessWrong",
                        field: big_field.hash(),
                        reason: "NotBuddha"
                    });
                };
            } else {
                next.html = `Waiting the Master to judge ${student}'s guess.  The guess was: ${guess}.`;
            }
        }
        if (game_stage.stage == "StudentWon") {
            // Clear the guess for the next round
            $("#enterguess").val("");

            // And setup the display
            next.header_text=`${student} won!`;
            next.seconds_left = seconds;
            next.html = `
            The rule was: <i>${next.rule}</i>.
            <br>
            ${student} entered it as <i>${guess}</i>
            <br>
            <button id="volunteer">I want to be Master next</button>
            `;
            next.finishing_function = function() {
                $("#volunteer").off("click").click(function() {
                    submit_command( {
                        command: "VolunteerForMaster",
                    });
                });
            };
        }
    }
}

var error_timeout = null;
function submit_command(obj) {
    obj.secret = game.password();
    // Send the command
    $.post("/command",JSON.stringify(obj),
        // If we succeed, clear any error we had
        function() {
            if (error_timeout != null) {
                clearTimeout(error_timeout);
                $("#error-text").addClass("hidden");
                error_timeout = null;
            }
        }, "text")
        .fail(function(e){
            // If we fail, show the error
            let response = JSON.parse(e.responseText);
            $("#error-text").html("Error: " + response.error).removeClass("hidden");

            // And set a timeout to clear the error again
            error_timeout = setTimeout(function() {
                $("#error-text").addClass("hidden");
                error_timeout = null;
            }, 5000);
        });
}


/*!
 * Starting externally copied code
 * Dynamically changing favicons with JavaScript
 * Works in all A-grade browsers except Safari and Internet Explorer
 * Demo: http://mathiasbynens.be/demo/dynamic-favicons
 */

// HTML5™, baby! http://mathiasbynens.be/notes/document-head
document.head || (document.head = document.getElementsByTagName("head")[0]);

function changeFavicon(src) {
    var link = document.createElement("link"),
        oldLink = document.getElementById("dynamic-favicon");
    link.id = "dynamic-favicon";
    link.rel = "shortcut icon";
    link.href = src;
    if (oldLink) {
        document.head.removeChild(oldLink);
    }
    document.head.appendChild(link);
}

// Back to my own code

var big_field;
var game;
var interval_ref; // used to clear the interval

$(document).ready(function() {
    game = new Game();

    // Get salt for security
    $.get("/salt", "", function(data, ts, jq) {
        game.salt = data;
    }, "text");

    // Create main field
    big_field = new Field("soulwindow","0","hashfield", true);
    big_field.draw();

    $("#resetbutton").click(function() {
        big_field.reset();
        big_field.draw();
    });

    $("#reject-unclear").click(function() {
        // If master -> buddha
        // If not master -> mondo
        submit_command({
            command: "JudgeGuessWrong",
            field: big_field.hash(),
            reason: "Undecidable"
        });
    });

    $("#loginbutton").click(function() {
        if (game.salt == null) {
            // If we don't have a salt yet, don't continue
            return;
        }
        // Create the user
        submit_command({
            command: "AddUser",
            username: $("#username").val(),
        });

        // Setup fetch of data every 2 seconds
        clearInterval(interval_ref);
        interval_ref = setInterval(function(){
            $.get("/user/"+game.password()+"/status",null, function(data, stat, jqxhr) {
                game.handle_status(data);
            });
        }, 2000);
    });

    $("#skip").click(function() {
        submit_command( { command: "Skip" });
    });
});

