var salt = null;

function password() {
    var pass_raw = $("#password").val();
    return sha256(pass_raw + salt);
}

var me_master = false;
var me_turn = false;

function handle_status(stat) {
    // Handle user list
    var userlist = "";
    me_master = false;
    me_turn = false;
    for (i in stat.users) {
        var u = stat.users[i];
        userlist += "<li>"+u.username;
        // Display win stars
        userlist += "&#x2b50;".repeat(u.wins);
        
        // Display guess icons
        userlist += "&#x1f4a0;".repeat(u.guess_tokens);

        // Display master Icon
        if (u.master) {
            userlist += "&#x1f5ff;";
            if (u.me) {// if I am the master
                me_master = true;
            }
        }
        // Display turn arrow
        if (u.turn) {
            userlist += "&#x21e6;";
            if (u.me) {// if I am the master
                me_turn = true;
            }

        }
        userlist += "</li>";
    }
    $("#userlist").html(userlist);

    var i;
    // Handle buddha list
    // Is there anthing there that shouldn't be?
    for (i in buddha_hashes) {
        if (!stat.buddha.includes(buddha_hashes[i])) {
            // If there are any missing
            // Burn it all!
            buddha_hashes = Array();
            $("#good").html("")
        }
    }
    for (i in not_buddha_hashes) {
        if (!stat.notbuddha.includes(not_buddha_hashes[i])) {
            // If there are any missing
            // Burn it all!
            not_buddha_hashes = Array();
            $("#bad").html("")
        }
    }

    // Write in new
    for (i in stat.buddha){
        var b = stat.buddha[i];
        if (!buddha_hashes.includes(b)) {
            buddha_hashes.push(b);
            add_example("good", b, f);
        }
    }
    for (i in stat.notbuddha){
        var b = stat.notbuddha[i];
        if (!not_buddha_hashes.includes(b)) {
            not_buddha_hashes.push(b);
            add_example("bad", b, f);
        }
    }

}

var f;
var interval_ref; // used to clear the interval
var buddha_hashes = Array();
var not_buddha_hashes = Array();
$(document).ready(function() {
    $.get("/salt", "", function(data, ts, jq) {
        salt = data;
    }, "text");
    f = new Field("soulwindow","e8e8e8e8e8e8e8e8e8e8e8e8e8e8e8e8e8e8e8e8e8e8e8e8e8","hashfield", true);
    f.draw();

    $("#resetbutton").click(function() {
        f.reset();
        f.draw();
    });

    $("#gamebutton1").click(function() {
        var command = {
            command: "SubmitFieldAsMaster",
            field: f.hash(),
            secret: password(),
            buddha: true
        };
        $.post("/command",JSON.stringify(command), null, "text");
    });

    $("#gamebutton2").click(function() {
        var command = {
            command: "SubmitFieldAsMaster",
            field: f.hash(),
            secret: password(),
            buddha: false 
        };
        $.post("/command",JSON.stringify(command), null, "text");
    });

    $("#loginbutton").click(function() {
        var login = {
            username: $("#username").val(),
            secret: password()
        };

        $.post("/adduser",JSON.stringify(login), null, "text");

        clearInterval(interval_ref);
        interval_ref = setInterval(function(){
            $.get("/user/"+password()+"/status",null, function(data, stat, jqxhr) {
                handle_status(data);
            });
        }, 2000);
    });

    $("#guess_entry").addClass("invisible");

    $("#submitguess").click(function() {
        var judgement = {
            secret: password(),
            guess: $("#enterguess").val()
        };
        $.post("/submitguess",JSON.stringify(judgement), null, "text");
    });
    $("#skipguess").click(function() {
        $.post("/user/" + password() + "/skip", "", null, "text");
    });
});
